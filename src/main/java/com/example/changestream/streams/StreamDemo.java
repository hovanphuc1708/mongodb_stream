package com.example.changestream.streams;


import com.example.changestream.entity.PriceOpen;
import com.mongodb.client.MongoChangeStreamCursor;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.client.result.UpdateResult;
import org.bson.*;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;

@Service
public class StreamDemo {
    @Autowired
    private MongoDatabase Collections;

    @Autowired
    public void Demo() {
        //Autowired function để được gọi khi run application
        DateOpenStream1();
    }

    //Listener event insert/update của dateOpen + tourPrice collection để generated data tương ứng vào priceOpen collection
        //1. Tạo con trỏ và open change streams cho db
            //- Filters operationType gồm: insert, update
            //- Filter collections gồm: dateOpen, tourPrice
        //2. Chạy con trỏ và handle events
        //3. Xác định events diễn ra ở collection nào được chỉ đỉnh để update data tương ứng vào priceOpen collection
            //Nếu event diễn ra ở colletion dateOpen => update dateOpen theo tourId
            //Nếu event diễn ra ở collection tourPrice => update currency + price theo tourId
            //* UpdateOptions upsert(true)
    public void DateOpenStream1(){
        MongoCollection<PriceOpen> mongoCollection = Collections.getCollection("priceOpen",PriceOpen.class);
        List<Bson> pipeline = new ArrayList<>();

        //Filters to events chỉ định
        Bson match = Aggregates.match(Filters.in("operationType",asList("insert", "update")));
//        Bson match = Aggregates.match(Filters.in("operationType",asList("insert", "update", "delete")));
        //Filters to collections chỉ định
        Bson collection = Aggregates.match(Filters.in("ns.coll", asList("dateOpen", "tourPrice")));

        pipeline.add(match);
        pipeline.add(collection);

        MongoChangeStreamCursor<ChangeStreamDocument<Document>> cursor
                = Collections.watch(pipeline).fullDocument(FullDocument.UPDATE_LOOKUP).cursor();
//                = Collections.watch(pipeline).fullDocument(FullDocument.UPDATE_LOOKUP).fullDocumentBeforeChange(FullDocumentBeforeChange.DEFAULT).cursor();

        while (cursor.hasNext()) {
            ChangeStreamDocument<Document> event = cursor.next();
            //Get collection diễn ra event
            String eventCollection = Objects.requireNonNull(event.getNamespace()).getCollectionName();
            System.err.println("Database: " + eventCollection); //Kiểm tra collection diễn ra event
            System.err.println("OperationType: " + event.getOperationTypeString()); //OperationType event
            System.out.println(event.getFullDocument()); //Kiểm tra data sau khi update

            if (eventCollection.equals("dateOpen")){
                String tourId = event.getFullDocument().get("tourId").toString();
                List<String> dateAvailable = event.getFullDocument().getList("dateAvailable", String.class);

                Bson filter = Filters.eq("tourId", new ObjectId(tourId));
                Bson newValue = new Document("dateOpen", dateAvailable).
                        append("tourId",  new ObjectId(tourId));
                Bson updates = new Document("$set", newValue);
                UpdateOptions op = new UpdateOptions().upsert(true);

                UpdateResult rs = mongoCollection.updateOne(filter, updates, op);
                System.out.println("ModifiedCounts: "+rs.getModifiedCount()); //Check modified priceOpenCollection
            }

            if (eventCollection.equals("tourPrice")){
                String tourId = event.getFullDocument().get("tourId").toString();
                Double price = event.getFullDocument().getDouble("price");
                String currency = event.getFullDocument().getString("currency");

                Bson filter = Filters.eq("tourId", new ObjectId(tourId));
                Bson newValue = new Document("currency", currency)
                        .append("tourId",  new ObjectId(tourId))
                        .append("price", price);
                Bson updates = new Document("$set", newValue);
                UpdateOptions op = new UpdateOptions().upsert(true);

                UpdateResult rs = mongoCollection.updateOne(filter, updates, op);
                System.out.println("ModifiedCounts: "+rs.getModifiedCount()); //Check modified priceOpenCollection
            }

        }
    }

}
