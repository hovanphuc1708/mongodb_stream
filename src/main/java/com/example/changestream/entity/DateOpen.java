package com.example.changestream.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "dateOpen")
public class DateOpen {
    @Id
    private ObjectId id;
    private ObjectId tourId;
    private List<String> dateAvailable;
    private Boolean status;
}
